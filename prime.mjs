const numbersChecked = {
    1: false,
    2: true,
    3: true,
    4: false,
    5: true,
    6: false,
    7: true,
    8: false,
    9: false,
    10: false
};

export default function isPrime(num) {
    if (num in numbersChecked) {
        return numbersChecked[num];
    } else {
        if ((num % 3===0 || num % 2 === 0)) { 
            numbersChecked[num]=false;
            return false;
        }
        const sqrt = Math.sqrt(num);
        for (let f=2;f<sqrt; f++) {
            if (!isPrime(f)) {
                continue;
            }
            if (num % f===0) {
                numbersChecked[num] = false;
                return false;
            }
        }
        numbersChecked[num] = true;
        return true;
    }
    throw new Error(`Shouldn't be able to get here`);
}