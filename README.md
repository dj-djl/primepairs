# MPMP: Primepairs #

Matt Parker sets the puzzle here:
https://www.youtube.com/watch?v=AXfl_e33Gt4

TL;DW - Find an arrangement of the numbers 1-9 such that every pair of adjacent numbers sums to a prime number.

This single-threaded node application checks all possible arrangements by brute-force.

### Implementation ###

I opted not to use any libraries for this, which means I need to do everything from scratch.
The first problem is how to find all possible arrangements:

I achieved this with a simple algorithm:

example, for the numbers 1-4:

1. take the last first and try it in all positions 1234 2134 2314 2341
1. now move the 2nd digit by one place to the right (moving it left is the same as one of the above sequences and try the first digit in all positions again 1324 3124 3214 3241 
1. move the 2nd digit again, and try the first digit in all positions again 1342 3142 3412 3421
1. now we move the 3rd digit and try all the above again 1243 2143 2413 2431   1423 4123 4213 4231    1432 4132 4312 4321
1. 4th digit has nowhere to go - we're done.


Next problem is how to test for primes?

The simple answer to this is to check all numbers below the number being checked and see if it's divisible, but there are some optimisations:

* Firstly, a number can't ever be divisible by anything greater than its squareroot (except the number itself), so we can ignore anything > sqrt(num)

* Secondly, we only need to check primes, since if its divisible by a non-prime then it must also be divisible by a smaller prime (that has already been checked).

* I added a quick check for divisibility by 2 & 3 since these are the most common cases.

* I pre-calculated the first 10 numbers to avoid some edge cases

* Finally, there is no need to test a number for primality twice, so the result of each calculation is cached.

### Running ###

You'll need node installed, I'm using version 10, but any later version should also work, older versions may or may not work.

You can just do `./index.mjs` to run, or if you prefer - `node --experimental-modules index.mjs`

### FAQ ###

* What is this junk at the top of the file?

  [shebangs](https://en.wikipedia.org/wiki/Shebang_(Unix)) don't support arguments, and I needed to pass the `--exprimental-modules` argument to node, the first 4 lines achieve exactly that, without being a syntax error for node.

  I could have just included a single line shell-script instead, but where is the fun in that?

  Besides, it was originally just a single file, before I split the prime test into a separate file.

* why node?

  why not?

* Can this be run in a browser?

  With some minor changes - probably, I haven't tried it. Performance is likely to be worse than in node though.

### Conclusions ###

Submitable puzzle:

There are 362880 arrangements of 9 numbers, of which 140 are prime-pairs

It took my PC 0.108 seconds to determine this.

Further analysis:

none yet - each increase in the count of numbers to be arranged takes exponential time to test, but the number of prime-pair arrangements is not exponential - proof: 

``` 
checked 24 arrangements of 4 numbers in 0s, of which **8** are prime-pairs
checked 120 arrangements of 5 numbers in 0s, of which **4** are prime-pairs
```

outputs so far:

```
checked 2 arrangements of 2 numbers in 0s, of which 2 are prime-pairs
checked 6 arrangements of 3 numbers in 0s, of which 2 are prime-pairs
checked 24 arrangements of 4 numbers in 0s, of which 8 are prime-pairs
checked 120 arrangements of 5 numbers in 0s, of which 4 are prime-pairs
checked 720 arrangements of 6 numbers in 0.004s, of which 16 are prime-pairs
checked 5040 arrangements of 7 numbers in 0.009s, of which 24 are prime-pairs
checked 40320 arrangements of 8 numbers in 0.033s, of which 60 are prime-pairs
checked 362880 arrangements of 9 numbers in 0.108s, of which 140 are prime-pairs
checked 3628800 arrangements of 10 numbers in 1.119s, of which 1328 are prime-pairs
checked 39916800 arrangements of 11 numbers in 13.136s, of which 2144 are prime-pairs
checked 479001600 arrangements of 12 numbers in 150.953s, of which 17536 are prime-pairs
checked 6227020800 arrangements of 13 numbers in 1880.68s, of which 40448 are prime-pairs
```

### Next steps ###

* Add threading so that more combinations can be checked in less time.

### Licence ###

Do what you want with this code, but don't blame me if it's wrong or breaks anything, and don't try claim that you wrote it