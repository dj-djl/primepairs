#!/bin/bash
true /*
node --experimental-modules $0
exit 0
*/

import isPrime from './prime.mjs'

let p=0;
const progress = '.oO0°0Oo.';
const minCount = 2;
const maxCount = 100;
for (let count = minCount; count <= maxCount; count++) {
    const dtStart = new Date();
    // check all arrangements of the numbers 1-count where all the consecutive pairs sum to a prime number
    // first problem - how to find all arrangements?
    // example, for the numbers 1-4:
    // first, take the last first and try it in all positions 1234 2134 2314 2341
    // now move the 2nd digit by one place to the right (moving it left is the same as one of the above sequences and try the first digit in all positions again 1324 3124 3214 3241 
    // move the 2nd digit again, and try the first digit in all positions again 1342 3142 3412 3421
    // now we move the 3rd digit and try all the above again 1243 2143 2413 2431   1423 4123 4213 4231    1432 4132 4312 4321
    // 4th digit has nowhere to go - we're done.
    // thats a total of 24 orders. Google tells me there are 24 arrangements of 4 things - good!
    const numbers = (new Array(count)).fill(0).map((x, i) => i + 1);
    let arrangements=0;
    let primes=0;
    for (let arrangement of getArrangements(numbers)) {
        arrangements++;
        let allPrime = true;
        const sums=[];
        for (let i = 0; i < arrangement.length - 1; i++) {
            if (!isPrime(arrangement[i] + arrangement[i + 1])) {
                allPrime = false;
                break;
            }
            sums.push(arrangement[i] + arrangement[i + 1])
        }
        if (allPrime) {
            primes++;
            p=(p+1) % progress.length;
            process.stdout.write(`${progress[p]}\r`)
            //console.log('all pairs in this arrangement are prime:', arrangement.join(','), sums.join(','));
        }
    }
    console.log(`checked ${arrangements} arrangements of ${count} numbers in ${(new Date() - dtStart)/1000}s, of which ${primes} are prime-pairs`);
}

function* getArrangements(arr, pos = 0) {
    //console.log('X', arr, pos);
    if (pos >= arr.length) {
        yield arr;
        return;
    }
    for (const arr2 of getArrangements(arr, pos + 1)) {
        //   console.log('Y', pos, arr2);
        for (let idx = pos; idx < arr2.length; idx++) {
            yield [...arr2.slice(0, pos), ...arr2.slice(pos + 1, idx + 1), arr2[pos], ...arr2.slice(idx + 1)];
        }
    }
}

/*

HERE

// */